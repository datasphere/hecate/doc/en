.. index::
   single: howto

===============
Creating a view
===============

This document details how to create a view and how to use it.

Prerequisites
=============

The explanation provided here assumes that you are familiar with `Web Components <https://developer.mozilla.org/en/docs/Web/API/Web_components>`_, shadow DOM and have some knowledge on Javascript.
Optionally, some knowledge of the `D3.js library <https://d3js.org/>`_ could be useful.
Any javascript graphic library can be used, although, it is strongly advised to use an open-source one.

Steps
=====

Let's retrace all the steps needed to create a view.

#. **Create a Javascript web component**

   The web component allows the view to be used in various application.
   In the constructor, you will need to initialize the view's attributes.

   .. code-block:: javascript

     class ViewName extends HTMElement {
       constructor() {
         //initialization of attributes
         super();
         this.data = {};
         this.settings = {};
       }
     }

   In this example, I am setting up a view that consist of two attributes : ``data`` for input data, and ``settings`` for view configuration.

#. **Manage the attributes**

   You'll need to put in place a system to keep track of your attributes, I suggest using two functions.
   The first one will return an array of attributes observed by the browser.
   The second one will check for changes in the value of the attributes and render the changes when needed.

   Here is a simple example :

   .. code-block:: javascript

     static get observedAttributes() { return ['data', 'settings']; }

     attributeChangedCallback(property, old, now) {
       if (old === now) return;  // don't bother to render if there is nothing new
       this[property] = now;
     }

#. **Use** ``connectedCallBack``

   ``connectedCallback`` is a method invoked each time the web component element is added to the document.
   The specification recommends that developers implement custom element setup in the ``connectedCallback`` rather than in the constructor whenever possible.
   The reason for that is because the constructor is called when the element is first created, which may happen before it is connected to the DOM, potentially limiting access to certain resources or dependencies.
   With that being said, this method will be central for a series of other functions.
   Why? it'll be easier to maintain and test each function individually.

   Said functions will depend on the view you are implementing.
   However, there are many reocurring step to setting up a view:

   * ``createShadowDom`` : creates one or multiple HTML elements and appends them to the root of the shadowDOM. This prevents potential conflicts between the component's internal code and the global styles or scripts on the page, as you have effectively encapsulated the elements you've just created, thereby isolating them from the rest of the page. This ensures that the component behaves consistently regardless of the surrounding environment. This is particurlarly important considering that we want to use views in different web settings.
   * ``createScene`` : creates a svg element, defines its width and height, can add some margins etc
   * ``retrieve data``: retrieves and parses the data from a JSON string.
   * ``prepareData``: does any preliminary treatment to the data (sort, convert types, etc).
   * ``displayChart``: does the final display, when everything is set and ready.