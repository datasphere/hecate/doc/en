# Configuration file for the Sphinx documentation builder.

# -- Project information -----------------------------------------------------
language = 'en'

project = 'HECATE'
slug = 'hecate'
group = 'datasphere'
website = f'https://{group}.readthedocs.io/projects/{slug}'
mirror = f'https://{group}.gitpages.huma-num.fr/{slug}/doc/{language}/'
versions = f'https://readthedocs.org/projects/{slug}-ds/downloads/'
repo = f'https://gitlab.huma-num.fr/{group}/{slug}/doc/{language}'
issues = f'{repo}/issues/'
changelog = f'{repo}/commits/'

copyright = 'CC BY-NC-SA 3.0'
author = 'Régis Witz'

release = '1.0'
version = '1.0.0'

rst_prolog = """
.. |project| replace:: {project}
.. |website| replace:: {website}
.. |mirror| replace:: {mirror}
.. |repo| replace:: {repo}
.. |version| replace:: {version}
.. |versions| replace:: {versions}
.. |changelog| replace:: {changelog}
.. |issues| replace:: {issues}
.. |copyright| replace:: {copyright}
""".format(
  project=project,
  website=website,
  mirror=mirror,
  repo=repo,
  version=version,
  versions=versions,
  changelog=changelog,
  issues=issues,
  copyright=copyright,
)

# -- General configuration ---------------------------------------------------

extensions = [
  'sphinx.ext.autodoc',       # HTML generation from docstrings
  'sphinx.ext.intersphinx',   # Link to other projects’ documentation
]

intersphinx_mapping = {
  'python': ('https://docs.python.org/3/', None),
  'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_rtd_theme'

# see: https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html
html_static_path = ['../resource']
html_css_files = ['css/candy.css']
html_logo = 'https://sharedocs.huma-num.fr/wl/?id=zu51tqXtpv5e9lv8DXzu0Vrg9DLNKowD&fmode=download'
html_theme_options = {
  'logo_only': True,
  'style_external_links': True,
}

# -- Options for EPUB output -------------------------------------------------
epub_show_urls = 'footnote'
