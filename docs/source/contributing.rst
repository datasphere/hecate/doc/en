.. index::
   single: contributing
   single: help
.. _contributing:

contributing
============

You'd like to contribute to this documentation?
First of all: thank you!

- You found a typo that you would like to fix? ...
- You think that an explanation is bad, and would like to clarify it? ...
- You noticed a gap in the documentation that you would like to fill? ...
- You detected an obsolete information that you would like to update? ...
- You want to translate this documentation into another language? ...

... Your help is welcome!

| This project uses a ticket system to discuss the objectives and to track the remaining or already completed tasks.
 Therefore, please feel free to use these tickets.
 You can thus contribute to an existing discussion.
 You can also submit a request or better yet, offer your help.
| The ticket manager can be found at
| |issues|



How this stuff works
--------------------

Each page of this documentation is initially written in a plain text file.
The syntax used is reStructuredText [#rst-primer]_ [#rst-quick]_.
This is a fairly simple syntax, suitable for representing complex elements in a simple text file.
As an example, you can click on the "View page source" link that may be found at the top right of each page on the website: this will allow you to see how that page is written in reStructuredText.

Each time one of these files is modified, the Sphinx Documentation Generator [#sphinx]_ produces various output formats, among them a book in PDF or EPUB formats, as well as a website that makes it easier to :ref:`find any information <how_to_use_this>`.

The generated website is in turn hosted on *Read the Docs* [#readthedocs]_, a well-known documentation hosting service.

All this is flexible, based on free software, and perfectly reproducible.

.. note::
   **Why not use Markdown?**
   
   The principle of reStructuredText is similar to Markdown/CommonMark [#md-commonmark]_.
   However, by default, the latter does not support some elements such as tables, inserts or footnotes.
   In addition, Markdown is not suitable for automatic generation of tables of contents or :ref:`indexes <genindex>`.
   Finally, one of the aims of this document is resilience.
   However, Markdown suffers from glaring flaws at the conceptual level [#md-whats-wrong]_ [#md-mistakes]_, which seem to be a hindrance to this issue.

.. note::
   **Why not use pandoc?** [#pandoc]_
   
   I have nothing against pandoc.
   At some point choices must be made, that's all.
   You can also generate this documentation with pandoc, if you like.
   And if you do so, would you mind sharing it with the community ?

.. note::
   **Can Read the Docs be trusted?** [#readthedocs]_

   Read the Docs Community pricing offer is "always free" for open source projects like |project|.
   Of course, a promise is only as good as the one who makes it.
   However, this offer has been running since 2010, which is certainly to their credit.
   
   | *But what if Read the Docs suspends its offer?*
   | As explained above, the build system of this documentation is entirely reproducible.
     It could therefore easily be hosted elsewhere, for example in a research repository.
     It's also possible to host it nowhere in particular, since anyone can freely rebuild it.
   | Anyways, this will not make this documentation unavailable,
     since a mirror site already exists here:
     |mirror|

   | *And what if Read the Docs "steals" or "leaks" data?*
   | |project| is free software, and its documentation is no exception.
     It's a free gift, with no time limit, to anyone who wishes to use it.
     If that gives you a thrill, you can just imagine you're "stealing" it.
     Now you're such a meanie, heh?
     (づ｡◕‿‿◕｡)づ



.. rubric:: References

.. [#rst-primer] `reStructuredText Primer <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_
.. [#rst-quick] `Quick reStructuredText <https://docutils.sourceforge.io/docs/user/rst/quickref.html>`_
.. [#sphinx] `Official Sphinx website <https://www.sphinx-doc.org/>`_
.. [#readthedocs] `Read the Docs website <https://readthedocs.org/>`_
.. [#md-commonmark] `The CommonMark initiative <https://commonmark.org/>`_, one of the (too many) Markdown specifications.
.. [#md-whats-wrong] `What's Wrong with Markdown? <https://www.adamhyde.net/whats-wrong-with-markdown/>`_, by Adam Hyde.
.. [#md-mistakes] `6 Things Markdown Got Wrong <https://www.swyx.io/markdown-mistakes>`_ (swyx)
.. [#pandoc] `Official pandoc website <https://pandoc.org/>`_
