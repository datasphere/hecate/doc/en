.. index::
   single: introduction
.. |date| date::
.. _how_to_use_this:

========================
using this documentation
========================

The purpose of this documentation is to help you in your research.
However, it is also intended for anyone interested in data management in general, and research data in particular.

This documentation is available in several forms:
a :ref:`web site <website>`, and a :ref:`book <book>`.
In either form, it is published under the terms of the |copyright| license.

.. index::
   single: website
.. _website:

how to use this website
=======================

You can navigate through this site in different ways.
Choose the one(s) that suits you best!

#. You can discover this site as you would flip through the pages of a book, using the "Previous" and "Next" buttons.
   They are usually located at the bottom of each page.
#. You can go directly to the section you are interested in, using the table of contents.
   Depending on your device, this can be found on the left side of the screen, or in the menu bar at the top of the screen.
   You can also :ref:`browse it directly here <toc>`.
#. If you need an index, :ref:`it can be found over here <genindex>`.
#. You can also be guided by :ref:`this list of frequently asked questions <faq>`.
#. You can search for information directly using the search field.
   It is located at the top left of your screen.
#. If you prefer to read this in another language, use the `Read the Docs flyout menu <https://docs.readthedocs.io/en/stable/flyout-menu.html>`_.
   Depending on your device, this can be found at the bottom left of your screen, or at the bottom of the menu that opens when you click at the top left of your screen.

Obviously, this site is accessible from any device: PC, smartphone, tablet, ...

.. index::
   single: book
   single: EPUB
   single: PDF
.. _book:

how to use this book
====================

+This documentation is available as **PDF**, **EPUB** or **HTML (zip)**.
+You can download these different versions via the `Read the Docs flyout meny <https://docs.readthedocs.io/en/stable/flyout-menu.html>`_.

#. Table of contents begins :ref:`here <toc>`.
#. General index begins :ref:`here <genindex>`.
#. Frequently (or not so frequently) asked questions are listed :ref:`here <faq>`.

You can read, cite, print or redistribute this documentation under the terms of the |copyright| license.

.. note::
   You can download this documentation as a book `from the website <https://datasphere.readthedocs.io/>`_, using the `Read the Docs flyout menu <https://docs.readthedocs.io/en/stable/flyout-menu.html>`_.
   Depending on your device, this can be found at the bottom left of your screen, or at the bottom of the menu that opens when you click at the top left of your screen.

.. index::
   single: versions
   single: repository
   single: code
   single: source

latest version
==============

.. role:: raw-html(raw)
   :format: html

You are currently reading version |version| of this documentation, generated on |date|.
:raw-html:`<br/>`
The latest version of this document:

- Is available here :
  :raw-html:`<br/>`
  |website|
  :raw-html:`<br/>`
  |mirror| (mirror site)
- Can be downloaded here:
  :raw-html:`<br/>`
  |versions|
- All updates are listed here:
  :raw-html:`<br/>`
  |changelog|
- Source code is available here:
  :raw-html:`<br/>`
  |repo|

 ｡◕‿◕｡ ♪♫
