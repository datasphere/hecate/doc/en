.. index::
   single: FAQ
   single: help
   single: questions
.. _faq:

==========================
frequently asked questions
==========================

.. toctree::
   :caption: Need any help?
   :maxdepth: 1

   How to use this documentation? <design/this>
   How to contribute? <contributing>
